﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Mvc;
//using System.Security.Claims;
//using Microsoft.AspNetCore.Authentication;
//using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Identity;
//using Microsoft.AspNetCore.Mvc.Rendering;
//using Microsoft.Extensions.Logging;
//using Microsoft.Extensions.Options;
//using NBRHC.Models;
//using NBRHC.Models.AccountViewModels;
////using NBRHC.Services;

//namespace NBRHC.Controllers
//{
//     //???...........
//    [Authorize]
//    [Route("[controller]/[action]")]
//    public class AccountController : Controller
//    {

//        //This doesn't include emails...............@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//        //UserManager<class> is a class that managers user... part of identity core, 
//        //UserManager does CRUD for users and has methods to find a user by id, name, email
//        //Adds and removes roles, and generates password hash and validates user

//        //Sign in manager is class that handles the user sign in from the application 
//        //Responsible for authenticating a user (signing a user in and out, and issues authentication cookie to user)

//        //Use dependency injection to get UserManager and SignIn Manager in the constructor of Account Controller????!
//        //huuuhhh

//        private readonly UserManager<ApplicationUser> _userManager;
//        private readonly SignInManager<ApplicationUser> _signInManager;
//        private readonly ILogger _logger;

//        public AccountController(
//                    UserManager<ApplicationUser> userManager,
//                    SignInManager<ApplicationUser> signInManager,
//                    ILogger<AccountController> logger)
//        {
//            _userManager = userManager;
//            _signInManager = signInManager;
//            _logger = logger;
//        }

//        //-------------------------------- Register action methods
//        [HttpGet]
//        [AllowAnonymous]
//        public IActionResult Register(string returnUrl = null)
//        {
//            ViewData["ReturnUrl"] = returnUrl;
//            return View();
//        }

//        [HttpPost]
//        [AllowAnonymous]
//        [ValidateAntiForgeryToken]
//        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
//        {
//            ViewData["ReturnUrl"] = returnUrl;
//            if (ModelState.IsValid)
//            {
//                //Creates a new application user instance for the user input 
//                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };

//                //uses CreateAsync method of user manager to create the user in the db
//                //creates the specified user in the db with the given password, as an asynchronous operation
                
//                var result = await _userManager.CreateAsync(user, model.Password);

//                //result is type IdentityResult, has 2 properties: 
//                //1) Succeed which is boolean value, 2)errors property, which is a collect of errors
//                if (result.Succeeded)
//                {
//                    _logger.LogInformation("User created a new account with password.");

//                    //if user is registered successfully, calls method SignInAync to sign in user
//                    await _signInManager.SignInAsync(user, isPersistent: false);
//                    _logger.LogInformation("User created a new account with password.");
//                    return RedirectToLocal(returnUrl);
//                }
//                AddErrors(result);
//            }

//            // If we got this far, something failed, redisplay form
//            return View(model);
//        }

//        //-------------------------- end of register action method 

//        //----------------- Login//

//        [HttpGet]
//        [AllowAnonymous]
//        public async Task<IActionResult> Login(string returnUrl = null)
//        {
//            // Clear the existing external cookie to ensure a clean login process
//            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

//            ViewData["ReturnUrl"] = returnUrl;
//            return View();
//        }

//        [HttpPost]
//        [AllowAnonymous]
//        [ValidateAntiForgeryToken]
//        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
//        {
//            ViewData["ReturnUrl"] = returnUrl;
//            if (ModelState.IsValid)
//            {
//                // This doesn't count login failures towards account lockout
//                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
//                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
//                if (result.Succeeded)
//                {
//                    _logger.LogInformation("User logged in.");
//                    return RedirectToLocal(returnUrl);
//                }
//                if (result.IsLockedOut)
//                {
//                    _logger.LogWarning("User account locked out.");
//                    return RedirectToAction(nameof(Lockout));
//                }
//                else
//                {
//                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
//                    return View(model);
//                }
//            }

//            // If we got this far, something failed, redisplay form
//            return View(model);
//        }

//        //----------------- End of Login//
//        //-------------------------------------region Helpers/Helper methods

//        private void AddErrors(IdentityResult result)
//        {
//            foreach (var error in result.Errors)
//            {
//                ModelState.AddModelError(string.Empty, error.Description);
//            }
//        }

//        private IActionResult RedirectToLocal(string returnUrl)
//        {
//            if (Url.IsLocalUrl(returnUrl))
//            {
//                return Redirect(returnUrl);
//            }
//            else
//            {
//                return RedirectToAction(nameof(HomeController.Index), "Home");
//            }
//        }

//        //----------------------------------------------endregion

//    }
//}