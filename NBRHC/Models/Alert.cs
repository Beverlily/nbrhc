﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBRHC.Models
{
    public class Alert
    {
        [Key]
        public int id { get; set; }

        //alert has 1 category
        [Required, StringLength(255), Display(Name = "Category")]
        public string category { get; set; }

        //Alert has 1 description
        [Required, StringLength(255), Display(Name = "Alert")]
        public string description { get; set; }

        //Alert has 1 start time
        [StringLength(255), Display(Name = "Start time")]
        public string startTime { get; set; }

        //Alert has 1 end time
        [StringLength(255), Display(Name = "End time")]
        public string endTime { get; set; }

        //Alert has one-to-one relationship with emergencies
        [Required, Display(Name = "Emergency"), ForeignKey("Emergency")]
        public Emergency emergency { get; set; }
    }
}
