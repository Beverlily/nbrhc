﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBRHC.Models
{
    public class DonationGoal
    {
        [Key]
        public int id { get; set; }

        //Describes the amount of money for the donation goal.
        [Required, Display(Name = "Donation goal")]
        public double goal { get; set; }

        //Each donation goal has a start date.
        [Required, StringLength(255), Display(Name = "Start date")]
        public string startDate { get; set; }

        //Each donation goal has an end date but it might not have been reached yet.
        [StringLength(255), Display(Name = "End date")]
        public string endDate { get; set; }

        //Double number between 0 and 100. Represents the progress in %.
        [Required, Display(Name = "Progress")]
        public double progress { get; set; }

        //Has the goal been met?
        [Required, Display(Name = "Was met")]
        public Boolean wasMet { get; set; }

        //The list of all the donors who have contributed to this donation goal.
        public virtual List<Donor> donors { get; set; }
    }
}
