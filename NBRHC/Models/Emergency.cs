﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBRHC.Models
{
    public class Emergency
    {
        [Key]
        public int id { get; set; }

        //Emergency has 1 category
        [Required, StringLength(255), Display(Name = "Category")]
        public string category { get; set; }

        //Emergency has 1 description
        [Required, StringLength(255), Display(Name = "Description")]
        public string description { get; set; }

        //Emergency has 1 start time
        [Required, StringLength(255), Display(Name = "Start time")]
        public string startTime { get; set; }

        //Emergency has 1 end time but it might not have been reached yet.
        [StringLength(255), Display(Name = "End time")]
        public string endTime { get; set; }

        //Emergency and Alert have a one-to-one relationship
        [Required, ForeignKey("Alert")]
        public Alert alert { get; set; }
    }
}
