﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using System.Threading.Tasks;??????????????????????????????????????
//using System.Data.Entity;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBRHC.Models
{
    public class Admin
    {
        [Key]
        public int AdminID { get; set; }
        public string AdminFirstName { get; set; }
        public string AdminLastName { get; set; }

        //This is a one to one relationship between admin and users 
        //In this case the UserID is a string in AspNetUsers
        [ForeignKey("UserID")]
        public string UserID { get; set; }

        public virtual ApplicationUser user { get; set; }

    }
}
