﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBRHC.Models
{
    public class VolunteerApplicant
    {
        [Key]
        public int id { get; set; }

        [Required, StringLength(255), Display(Name = "First name")]
        public string firstName { get; set; }

        [Required, StringLength(255), Display(Name = "Last name")]
        public string lastName { get; set; }

        [StringLength(255), Display(Name = "Cell number")]
        public string cellPhoneNo { get; set; }

        [StringLength(255) Display(Name = "Home phone number")]
        public string homePhoneNo { get; set; }

        [Required, StringLength(255), Display(Name = "Email address")]
        public string email { get; set; }

        [Required, StringLength(255), Display(Name = "City")]
        public string city { get; set; }

        [Required, StringLength(255), Display(Name = "Street number")]
        public string streetNumber { get; set; }
    }
}
