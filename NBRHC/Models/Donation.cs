﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBRHC.Models
{
    public class Donation
    {
        [Key]
        public int id { get; set; }

        
        //Each donation was made by a donor
        [Required, Display(Name = "Donor") ForeignKey("Donor")]
        public Donor donor { get; set; }

        //Each donation has an amount
        [Required, Display(Name = "Amount")]
        public double amount { get; set; }

        //Each donation has a date
        [Required, StringLength(255), Display(Name = "Date")]
        public string date { get; set; }

        //Each donation is made while one donation goal is active,
        //no two donation goals can be active at the same time.
        [Required, ForeignKey("DonationGoal"), Display(Name = "Donation goal")]
        public virtual DonationGoal donationgoal { get; set; }
    }
}
