﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NBRHC.Models
{
    public class Donor
    {
        [Key]
        public int id { get; set; }

        //Each donor has a first name
        [Required, StringLength(255), Display(Name = "First name")]
        public string firstName { get; set; }

        //Each donor has a last name
        [Required, StringLength(255), Display(Name = "Last name")]
        public string lastName { get; set; }

        //Each donor has an address
        [Required, StringLength(255), Display(Name = "Address")]
        public string address { get; set; }

        //Donor's home phone. Not required.
        [StringLength(255), Display(Name = "Home phone number")]
        public string homePhoneNo { get; set; }

        //Donor's cell phone. Not required.
        [StringLength(255), Display(Name = "Cell phone number")]
        public string cellPhoneNo { get; set; }

        //List of all the donations the donor has made.
        public virtual List<Donation> donations { get; set; }
    }
}
